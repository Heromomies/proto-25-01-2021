﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class GOT_Player : MonoBehaviour
{
    private static GOT_Player _instance;
    public static GOT_Player instance => _instance;
    private int _scoreValue = 0;
    
    public Image shieldImage, attackImage;
    public float dashSpeed;
    public float startDashTime;
    public int life;
    public GameObject explosionPlayerDash, panelEnd;
    public float speedBackDash;
    public TextMeshProUGUI scoreText;
    public Spawn_Manager sm;
    public Sprite[] sprite;
    public Transform initialPosition;
    public Slider sliderDash;
    public float sliderValue;
    
    private bool _isInputShield, _isInputAttack, _playerCanMove, _playerCanShield;
    private Rigidbody2D _rb;
    private float _dashTime;
    private int _direction;
    private int _numberDash;
    
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _dashTime = startDashTime;
        _instance = this;
    }

    void Update()
    {
        sliderDash.value = sliderValue;
        if (_direction == 0)
        {
            if (Input.GetKeyDown(KeyCode.Z) && _numberDash == 0)
            {
                GameObject ex = Instantiate(explosionPlayerDash, transform.position, Quaternion.identity);
                Destroy(ex, 0.5f);
                Camera.main.DOShakePosition(0.2f, 0.5f, 100, 10f);
                Camera.main.DOShakeRotation(0.2f, 0.5f, 100, 10f);
                _direction = 1;
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite[1];
                attackImage.GetComponent<Image>().color = Color.red;
            }
        }
        else
        {
            if (_dashTime <= 0)
            {
                _direction = 0;
                _dashTime = startDashTime;
                _rb.velocity = Vector2.zero;
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite[2];
                StartCoroutine(WaitingReturnInitialPosition());
            }
            else
            {
                _dashTime -= Time.deltaTime;
                if (_direction == 1)
                {
                    _rb.velocity = Vector2.right * dashSpeed;
                }
            }
        }
        if (Input.GetKey(KeyCode.Z) && !_isInputShield && _numberDash == 0)
        {
            Attack();
        }
        else
        {
            _isInputAttack = false;
            attackImage.GetComponent<Image>().color = Color.yellow;
        }

        if (Input.GetKey(KeyCode.A) && !_isInputAttack && _playerCanShield)
        {
            Shield();
            sliderValue -= .5f;
            if (sliderValue <= 0f)
            {
                sliderValue = 0;
                _playerCanShield = false;
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite[0];
            }
        }
        else
        {
            sliderValue += .2f;
            if (sliderValue > sliderDash.maxValue)
            {
                sliderValue = sliderDash.maxValue;
                _playerCanShield = true;
            }
            _isInputShield = false;
            shieldImage.GetComponent<Image>().color = Color.yellow;
        }
        if (Input.GetKeyDown(KeyCode.A) && _playerCanShield)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite[4];
        }
        else if(Input.GetKeyUp(KeyCode.A))
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite[0];
        }
        if (life <= 0)
        {
            panelEnd.SetActive(true);
            Time.timeScale = 0f;
        }

        if (_playerCanMove) // Access to the player to dash and to backdash
        {
            transform.Translate(Vector2.left * (Time.deltaTime * speedBackDash));
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite[3];
            if (transform.position.x <= initialPosition.position.x - 0.5)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite[0];
                _playerCanMove = false;
                if (transform.position.x <= initialPosition.position.x)
                {
                    gameObject.transform.position = initialPosition.position;
                }
                _numberDash = 0;
               
            }
        }
        scoreText.text = _scoreValue.ToString();
    }

    public void Attack()
    {
        _isInputAttack = true;
        _numberDash++;
        
        attackImage.GetComponent<Image>().color = Color.red;
    }

    public void Shield()
    {
        _isInputShield = true;
        shieldImage.GetComponent<Image>().color = Color.green;
    }

    public void Damage(int amount)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite[5];
        Camera.main.DOShakePosition(0.2f, 0.5f, 100, 10f);
        Camera.main.DOShakeRotation(0.2f, 0.5f, 100, 10f);
        if (!_isInputShield)
        {
            life -= amount;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _scoreValue += 20;
        sm.Enemy_retrieveFromList(other.gameObject);
        Destroy(other.gameObject);
    }

    IEnumerator WaitingReturnInitialPosition()
    {
        yield return new WaitForSeconds(0.3f);
        _playerCanMove = true;
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite[0];
    }
}

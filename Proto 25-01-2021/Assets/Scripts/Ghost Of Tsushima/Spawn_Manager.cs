﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Manager : MonoBehaviour
{
    public List<GameObject> enemy_list;

    public GameObject enemy_obj;
    
    public float waitTimer;
    private float maxTimer = 1f;
    private int number;

    public float gameTimer = 10f;
    public static int indexSpeed;

    private static Spawn_Manager _instance;
    public static Spawn_Manager spawnManager => _instance;
    // Start is called before the first frame update
    private void Awake()
    {
        _instance = this;
        indexSpeed = 0;
     
    }

    void Start()
    {
        waitTimer = maxTimer;
        waitTimer = Mathf.Clamp(waitTimer, 0, Mathf.Infinity);
        gameTimer = Mathf.Clamp(gameTimer, 0, Mathf.Infinity);
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        gameTimer -= Time.deltaTime;

        if (gameTimer <= 0)
        {
            indexSpeed++;
            gameTimer = 10f;
        }

        switch (indexSpeed)
        {
            case 0:
                maxTimer = 0.8f;
                break;
            case 1 :
                maxTimer = 0.6f;
                break;
            case 2:
                maxTimer = 0.4f;
                break;

        }
        
        
        waitTimer -= Time.deltaTime;
        
        if (waitTimer <= 0) 
        {
            while (enemy_list.Count <= number)
        {
            SpawnEnemy();
        }
            waitTimer = maxTimer;
        }
    }

    void SpawnEnemy()
    {
       GameObject enemy = Instantiate(enemy_obj, transform.position, Quaternion.identity);
       enemy_list.Add(enemy);
       
    }

    public void Enemy_retrieveFromList(GameObject enemy)
    {
        enemy_list.Remove(enemy);
    }
}

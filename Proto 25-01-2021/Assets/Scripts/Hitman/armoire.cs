﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class armoire : MonoBehaviour
{
    public GameObject player;
    public bool isHide;
    public float radius = 1f;
    public Transform endPosition;
    public float timer;

    private float maxtTimer = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        timer = maxtTimer;
        player = GameObject.FindGameObjectWithTag("Player");
        
    }

    // Update is called once per frame
    void Update()
    {
        timer = Mathf.Clamp(timer, 0, Mathf.Infinity);
        
        Hide();
        
        if (isHide && Input.GetKeyDown(KeyCode.E) && timer <= 0)
        {
            player.transform.position = endPosition.position;
            player.GetComponent<SpriteRenderer>().enabled = true;
            player.GetComponent<Rigidbody2D>().sleepMode = RigidbodySleepMode2D.StartAwake;
            player.GetComponent<BoxCollider2D>().enabled = true;
            player.GetComponent<playerMove>().enabled = true;
            isHide = false;
            timer = maxtTimer;
        }

        if (isHide)
        {
            timer -= Time.deltaTime;
        }
     
    }

    void Hide()
    {
        if (Vector2.Distance(gameObject.transform.position,player.transform.position) <= radius && Input.GetKeyDown(KeyCode.E) && !isHide)
        {
            player.GetComponent<SpriteRenderer>().enabled = false;
            player.GetComponent<Rigidbody2D>().sleepMode = RigidbodySleepMode2D.StartAsleep;
            player.GetComponent<BoxCollider2D>().enabled = false;
            player.GetComponent<playerMove>().enabled = false;
            player.transform.position = gameObject.transform.position;
            isHide = true;
           
        }
        
        
    }

    
}

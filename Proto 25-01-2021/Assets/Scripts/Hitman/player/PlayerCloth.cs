﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCloth : MonoBehaviour
{
    public Color color;
    private SpriteRenderer _SpriteRenderer;
    public GameObject cloth_anim;

   
    public void Start()
    {
        _SpriteRenderer = GetComponent<SpriteRenderer>();
        color = _SpriteRenderer.color;
    }

    IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<cloth>())
        {
            cloth_anim.SetActive(true);
            yield return new WaitForSeconds(1f);
            cloth_anim.SetActive(false);
            
            gameObject.GetComponent<SpriteRenderer>().color = other.gameObject.GetComponent<cloth>().color;
            color = gameObject.GetComponent<SpriteRenderer>().color;
            other.gameObject.GetComponent<cloth>().isChecked = true;
            GameManager._GameManager.CheckedCloth(other.gameObject.GetComponent<cloth>());
            other.gameObject.SetActive(false);
            yield return new WaitForSeconds(2f);
            other.gameObject.SetActive(true);
        }
    }
}

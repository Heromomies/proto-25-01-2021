﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMove : MonoBehaviour
{
    public float speed = 5f;

    private Vector2 _movement;

    private Rigidbody2D _rgb;

    private SpriteRenderer sprite_renderer;

    private bool _facingRight;
    // Start is called before the first frame update
    void Start()
    {
        sprite_renderer = GetComponent<SpriteRenderer>();
        _rgb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_movement.x >= 0)
        {
            _facingRight = true;
            sprite_renderer.flipX = false;
        }
        else
        {
            _facingRight = false;
            sprite_renderer.flipX = true;
        }
        
        _movement.x = Input.GetAxisRaw("Horizontal");
      
        _movement.y = Input.GetAxisRaw("Vertical");
    }

    private void FixedUpdate()
    {
        _rgb.MovePosition(_rgb.position + _movement * (speed * Time.fixedDeltaTime));
        //LookPositon();
    }
  /* void LookPositon()
    {
        Vector2 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
       // angle = Mathf.Clamp(angle, -180, 180);
        transform.rotation = Quaternion.AngleAxis(angle , Vector3.forward);
    }*/
}

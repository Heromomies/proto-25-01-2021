﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    public GameObject panelDead;
    
    private PlayerCloth _playerCloth;
    // Start is called before the first frame update
    void Start()
    {
        _playerCloth = GetComponent<PlayerCloth>();
    }

    public void Dead()
    {
        panelDead.SetActive(true);
        Time.timeScale = 0f;
    }

    public void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<Zone>() && other.gameObject.GetComponent<Zone>().color.ToString("0000") != _playerCloth.color.ToString("0000"))
        {
            Dead();
        }
    }
}
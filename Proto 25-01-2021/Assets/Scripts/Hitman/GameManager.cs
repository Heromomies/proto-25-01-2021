﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public List<cloth> _clothsLevels;
    
    public List<Zone> _zone;

    private static GameManager _instance;

    public static GameManager _GameManager => _instance;

    public PlayerLife _PlayerLife;
    // Start is called before the first frame update
    void Start()
    {
        _instance = this;
    }
    
    public void CheckedCloth(cloth cloth)
    {
        foreach (var zone in _zone)
        {
            
            if (zone.name == cloth.name)
            {
                zone._BoxCollider2D.isTrigger = true;
            }
            else if (zone.name != cloth.name || zone.color != Color.white)
            {
                zone._BoxCollider2D.isTrigger = false;
                
            }
           
        }
    }

    private void FixedUpdate()
    {
        Application.targetFrameRate = 60;
    }
}

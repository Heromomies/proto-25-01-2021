﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public GameObject zone;
    public bool isTouched;
    private float _timeLeft;
    public float maxTimer = 3f;

    private GameObject player;
    [SerializeField] 
    private Slider circle;

    public float maxAmount;
    public GameObject slider_go;
    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        _timeLeft = maxTimer;
        maxAmount = maxTimer;
        circle.maxValue = maxAmount;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            zone.GetComponent<BoxCollider2D>().isTrigger = true;
            zone.GetComponent<SpriteRenderer>().color = new Color(255,255,255, 0.2f);
            
            isTouched = true;
            
        }
    }
    
    public void Update()
    {
      
        
       
        if (isTouched)
        {
            slider_go.SetActive(true);
            zone.GetComponent<BoxCollider2D>().isTrigger = true;
            Debug.Log(isTouched);
            _timeLeft -= Time.deltaTime;
            circle.value -= Time.deltaTime;
            
            if (_timeLeft <= 0)
            {
                _timeLeft = Mathf.Clamp(_timeLeft, 0, Mathf.Infinity);
                isTouched = false;
                zone.GetComponent<SpriteRenderer>().color = zone.GetComponent<Zone>().color;
                zone.GetComponent<BoxCollider2D>().isTrigger = false;
                if (zone.GetComponent<SpriteRenderer>().color.ToString("0000") == player.GetComponent<PlayerCloth>().color.ToString("0000"))
                {
                    zone.GetComponent<BoxCollider2D>().isTrigger = true;
                }
                _timeLeft = maxTimer;
                circle.value = maxTimer;
                slider_go.SetActive(false);
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinPanel : MonoBehaviour
{
    public GameObject winPanel;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            winPanel.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void Menu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void NextLevel(int numberNextLevel)
    {
        Time.timeScale = 1f;
        switch (numberNextLevel)
        {
            case 1:
	            SceneManager.LoadScene(1);
	            break;
	        case 2:
		        SceneManager.LoadScene(2);
		        break;
	        case 3:
		        SceneManager.LoadScene(3);
		        break;
        }
    }
}

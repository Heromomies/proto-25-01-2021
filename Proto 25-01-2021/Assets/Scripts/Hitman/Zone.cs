﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Zone : MonoBehaviour
{
  public string name = "red";
  [HideInInspector] public BoxCollider2D _BoxCollider2D;
  [HideInInspector] public Color color;
  private SpriteRenderer sprite_color;
  public void Start()
  {
      sprite_color = GetComponent<SpriteRenderer>();
      _BoxCollider2D = GetComponent<BoxCollider2D>();
      color = sprite_color.color;
  }

 
}

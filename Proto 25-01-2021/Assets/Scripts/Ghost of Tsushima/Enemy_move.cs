﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Enemy_move : MonoBehaviour
{
    private Vector3 worldPos;
    private Vector3 movement;
    
    public float speed;

    public float distanceFromCaméra = 10f;
   

    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        worldPos = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, distanceFromCaméra));
        movement = new Vector3(worldPos.x, transform.position.y, worldPos.z);
        transform.position = Vector2.MoveTowards(transform.position, movement, speed * Time.deltaTime);
    }
}

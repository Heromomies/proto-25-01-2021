﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_shoot : MonoBehaviour
{
    public float waitTimer;
    private SpriteRenderer _spriteRenderer;
    private float maxTimer = 2f;
    public Sprite[] sprite_colors;
    public Sprite initial_sprite;
    public bool isShooting;

    public float waitColors = 0.4f;

    public Transform shootPoint;

    public LayerMask layerMask;

    public int index;
    // Start is called before the first frame update
    void Start()
    {
        waitTimer = maxTimer;
        _spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        index = Spawn_Manager.indexSpeed;
        waitTimer = Mathf.Clamp(waitTimer, 0, Mathf.Infinity);
        waitTimer -= Time.deltaTime;
        
        if (waitTimer <= 0 && !isShooting )
        {
            StartCoroutine(Shoot());
        }

        switch (Spawn_Manager.indexSpeed)
        {
            case 0:
                waitColors = 0.4f;
                maxTimer = 1.8f;
                break;
            case 1 : 
                waitColors = 0.3f;
                maxTimer = 1.5f;
            break;
            case 2 : 
                waitColors = 0.2f;
                maxTimer = 1.2f;
                break;
        }
    }

    IEnumerator Shoot()
    {
        isShooting = true;
        yield return new WaitForSeconds(waitColors);
        _spriteRenderer.sprite = sprite_colors[0];
        yield return new WaitForSeconds(waitColors);
        _spriteRenderer.sprite = sprite_colors[1];
        yield return new WaitForSeconds(waitColors);
        ShootRaycast();
        _spriteRenderer.color = Color.white;
        waitTimer = maxTimer;
        isShooting = false;
        _spriteRenderer.sprite =  initial_sprite;

    }


    void ShootRaycast()
    {
        RaycastHit2D raycastHit2D = Physics2D.Raycast(shootPoint.position, Vector2.left, 100f, layerMask);

        if (raycastHit2D)
        {
            Debug.DrawRay(shootPoint.position, Vector3.left * 100f, Color.red, 1f);
            GOT_Player.instance.Damage(1);
        }
    }
}

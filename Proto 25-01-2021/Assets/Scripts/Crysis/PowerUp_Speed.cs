﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUp_Speed : MonoBehaviour
{
    
    public Power powerUp;
    public Slider slider;
    
    
    public float sliderMaxValue = 3f;

    public playerMove playerMove;
    // Start is called before the first frame update
    void Start()
    {
        powerUp = new Power("Speed", 0f, 3f, false,slider);
        slider.maxValue = sliderMaxValue;
        slider.value = slider.maxValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && !powerUp.isActivated && !Power.isCanPowerUp)
        {
            StartCoroutine(SpeedUp());
        }
        
        
        if (powerUp.isActivated)
        {
            slider.value -= Time.deltaTime;
        }

        if (slider.value <=0)
        {
            powerUp.isActivated = false;
            slider.value = sliderMaxValue;
            Power.isCanPowerUp = false;
        }
    }

    IEnumerator SpeedUp()
    {
        powerUp.isActivated = true;
        Power.isCanPowerUp = true;
        playerMove.speed *= 2f;
        yield return new WaitForSeconds(3f);
        playerMove.speed /= 2f;
    }
}

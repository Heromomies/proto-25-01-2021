﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_weapon : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Crysis_Enemy>() && Input.GetMouseButton(0))
        {
            Destroy(other.gameObject);
        }
    }
}

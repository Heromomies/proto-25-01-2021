﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crysis_Enemy : MonoBehaviour
{
	public enum Direction {Right, Left, Up, Down};

	public LayerMask layerMask;

	public Direction myDirection;
	public float maxDistance;
	public float x,y,z;

	public PowerUp_Armure life;
	void Update()
	{
		if (myDirection == Direction.Right)
		{
			if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.right + new Vector3(x,y,z)), maxDistance, layerMask))
			{ 
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right + new Vector3(x,y,z)), Color.yellow);
				life.Damage();
				
			}
	
		}
		if (myDirection == Direction.Left)
		{
			
			if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.left + new Vector3(x,y,z)), maxDistance, layerMask))
			{ 
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.left + new Vector3(x,y,z)), Color.yellow);
				life.Damage();
				
			}
	
		}
		if (myDirection == Direction.Up)
		{
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector2.up + new Vector2(x,y)), maxDistance, layerMask))
			{
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up + new Vector3(x,y,z)), Color.yellow);
				life.Damage();
			
			}
		
		}
		if (myDirection == Direction.Down)
		{
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector2.down + new Vector2(x, y)),
				maxDistance, layerMask))
			{
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down + new Vector3(x, y, z)),
					Color.yellow);
				life.Damage();
			}

		}
	}
}

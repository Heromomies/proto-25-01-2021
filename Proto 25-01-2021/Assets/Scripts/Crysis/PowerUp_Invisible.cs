﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUp_Invisible : MonoBehaviour
{
    public Power powerUp;
    public Slider slider;

    public SpriteRenderer spriteRender;

    public Color color;

    public float sliderMaxValue = 2f;

    private BoxCollider2D _boxCollider2D;
    // Start is called before the first frame update
    void Start()
    {
        powerUp = new Power("Invisible", 0f, 2f, false,slider);
        _boxCollider2D = GetComponent<BoxCollider2D>();
        slider.maxValue = sliderMaxValue;
        slider.value = slider.maxValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && !powerUp.isActivated && !Power.isCanPowerUp)
        {
            StartCoroutine(Invisible());
        }

        if (powerUp.isActivated)
        {
            slider.value -= Time.deltaTime;
        }

        if (slider.value <=0)
        {
            powerUp.isActivated = false;
            slider.value = sliderMaxValue;
            Power.isCanPowerUp = false;
        }
    }




    IEnumerator Invisible()
    {
        powerUp.isActivated = true;
        Power.isCanPowerUp = true;
        color = spriteRender.color;
        color.a = 0.5f;
        spriteRender.color = color;
        _boxCollider2D.enabled = false;
        yield return new WaitForSeconds(2f);
        color.a = 1f;
        spriteRender.color = color;
        _boxCollider2D.enabled = true;
    }
}

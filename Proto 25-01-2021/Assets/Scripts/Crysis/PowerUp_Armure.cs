﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PowerUp_Armure : MonoBehaviour
{
    public Power powerUp;
    public Slider slider;
    
    
    public float sliderMaxValue = 4f;

    public bool canbeTouched;

    public int life;
    public int maxlife = 10;

    //public Slider life_slider;
    
    // Start is called before the first frame update
    void Start()
    {
        powerUp = new Power("Armure", 0, 4f, false, slider);
        slider.maxValue = sliderMaxValue;
        slider.value = slider.maxValue;
        life = maxlife;
       // life_slider.maxValue = maxlife;
        //life_slider.value = maxlife;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !powerUp.isActivated && !Power.isCanPowerUp)
        {
            StartCoroutine(Armure());
        }
            
        if (powerUp.isActivated)
        {
            slider.value -= Time.deltaTime;
        }

        if (slider.value <=0)
        {
            canbeTouched = false;
            powerUp.isActivated = false;
            slider.value = sliderMaxValue;
            Power.isCanPowerUp = false;
            Debug.Log("armored false !");
        }
    }

    IEnumerator Armure()
    {
        canbeTouched = true;
        powerUp.isActivated = true;
        Power.isCanPowerUp = true;
        Debug.Log("Armored !");
        yield return  new WaitForSeconds(4f);
    }

    public void Damage()
    {
        if (!canbeTouched)
        {
            life--;
            Debug.Log("Player life : " + life);
            //life_slider.value = life;
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Power
{
  public string name;
  public float timer;
  public float maxtimer = 2f;
  public bool isActivated;
  public Slider slider;
  public static bool isCanPowerUp;
  
  
  public Power(string name,float timer, float maxtimer, bool isActivated, Slider slider)
  {
    this.name = name;
    this.timer = timer;
    this.maxtimer = maxtimer;
    this.isActivated = isActivated;
    this.slider = slider;
  }

 
}
